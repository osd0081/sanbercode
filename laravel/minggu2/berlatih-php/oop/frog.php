<?php

class Frog extends Animal
{
    public $legs = 2;
    function __construct($name)
    {
        parent::__construct($name);
    }
    public function jump()
    {
        echo "Jump : Hop Hop<br>";
    }
    public function get_legs()
    {
        return $this->legs;
    }
}
