<?php

require('Animal.php');
require('Ape.php');
require('Frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->get_name() . "<br>";
echo "Legs : " . $sheep->get_legs() . "<br>";
echo "Cold Blooded : " . $sheep->get_coldBlooded() . "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->get_name() . "<br>";
echo "Legs : " . $kodok->get_legs() . "<br>";
echo "Cold Blooded : " . $kodok->get_coldBlooded() . "<br>";
echo $kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->get_name() . "<br>";
echo "Legs : " . $sungokong->get_legs() . "<br>";
echo "Cold Blooded : " . $sungokong->get_coldBlooded() . "<br>";
echo $sungokong->yell(); // "Auooo"
