<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use Illuminate\Routing\Router;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@register')->name('register');

Route::post('/welcome', 'AuthController@welcome');
