<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<body>
    <main>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="post">            
            @csrf
            <div class="namaForm">
                <label for="nf">First name:</label> <br><br>
                <input  id="nf" type="text" name="nameFirst"><br><br>
                <label for="nl">Last name:</label><br><br>
                <input id="nl" type="text" name="nameLast">
            </div><br>

            <div class="genderForm">
                <label>Gender:</label><br><br>
                <label for="male"><input id="male" value="male" type="radio" name="myGender">Male</label><br>
                <label for="female"><input id="female" value="female" type="radio" name="myGender">Female</label><br>
                <label for="other"><input id="other" value="other" type="radio" name="myGender">Other</label>
            </div><br>

            <div class="nationality">
                <label for="myNationality">Nationality:</label><br><br>
                <select list="nations" id="myNationality" name="myNationality">
                    <option value="Indonesian">Indonesian</option>
                    <option value="Malaysian">Malaysian</Main></option>
                    <option value="Australian">Australian</option>
                    <option value="Other">Other</option>
                </select>              
            </div><br>

            <div class="Languages">
                <label>Languages Spoken:</label><br><br>
                <label for="bahasaLang"><input id="bahasaLang" value="Bahasa Indonesia" type="checkbox" name="myLanguage">Bahasa Indonesia</label><br>
                <label for="englishLang"><input id="englishLang" value="English" type="checkbox" name="myLanguage">English</label><br>
                <label for="otherLang"><input id="otherLang" value="Other" type="checkbox" name="myLanguage">Other</label>
            </div><br>

            <div class="Bio">
                <label for="myBio">Bio:</label><br><br>
                <textarea id="myBio" name="myBio" rows="10" cols="30"></textarea>
            </div>
            
            <button type="submit">Sign Up</button>
                            
        </form>
    </main>
</body>

</html>