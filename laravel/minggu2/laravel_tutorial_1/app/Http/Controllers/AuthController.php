<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected function register()
    {
        return view('page.form');
    }

    protected function welcome(Request $request)
    {
        $firstName = $request["nameFirst"];
        $lastName = $request["nameLast"];
        return view('page.welcome', compact('firstName', 'lastName'));
    }
}
